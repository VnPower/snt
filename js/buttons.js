for (const button of CONFIG.buttons) {
  let item = "";
  // If IMG
  if (!button.icon.trim().startsWith("<svg")) {
    item = `
    <a title="${button.name}" href="${button.link}" class="button">
      <img src="${button.icon}" alt="${button.name}">
    </a>
    `;
  } else {
    // If SVG
    item = `
    <a title="${button.name}" href="${button.link}" class="button">
      ${button.icon}
    </a>
    `;
  }

  const position = "beforeend";

  links.insertAdjacentHTML(position, item);
}
