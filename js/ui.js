// CSS variable stuff

const root = document.querySelector(":root");

if (CONFIG.primaryColor != "") {
  root.style.setProperty("--primary", CONFIG.primaryColor);
}
if (CONFIG.baseColor != "") {
  root.style.setProperty("--base1", CONFIG.baseColor);
}

if (CONFIG.backgroundPath != "") {
  root.style.setProperty("--imgsrc", `url(${CONFIG.backgroundPath})`);
}

const backgroundDimValue = parseFloat(CONFIG.backgroundDim).toPrecision(2);
const backgroundBlurValue = parseFloat(CONFIG.backgroundBlur).toPrecision(2);

if (!isNaN(backgroundDimValue) && 0 <= backgroundDimValue <= 1) {
  root.style.setProperty(
    "--imgcol",
    `linear-gradient(rgba(0, 0, 0, ${backgroundDimValue}), rgba(0, 0, 0, ${backgroundDimValue}))`,
  );
}

if (!isNaN(backgroundBlurValue) && 0 <= backgroundBlurValue <= 1) {
  root.style.setProperty("--imgblur", `blur(${backgroundBlurValue}px)`);
}

//

const searchBar = document.querySelector("#search-form");
const searchText = document.querySelector("#search-text");
let searchEngine = CONFIG.searchEngine.trim();

if (searchEngine == "") {
  searchEngine = "duckduckgo.com";
}

searchBar.setAttribute("action", "https://" + CONFIG.searchEngine);
searchText.setAttribute("placeholder", "Search " + searchEngine);
