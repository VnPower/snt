# snt - simple/suckless new tab (wip)

![Catppuccin](https://ttm.sh/W7U.png)
![Nord](https://ttm.sh/W7l.png)

[Live preview](https://vnpower.codeberg.page/snt)

## Features

- Fast
- Lightweight (small codebase, 234 lines of code in v1.0)
- Hackable
- etc...

## Usage

#### Locally:

This is much more comfortable than the online method, since you will be able to use this offline.

**Chromium**:

1. Clone the repo
2. Go to `Extensions` and enable `Developer Mode` (top right)
3. `Load unpacked` (top left)
4. Choose the folder you just cloned
5. Enable the extension

**Firefox**:

1. Clone the repo
2. This Reddit post [here](https://libreddit.kavin.rocks/r/startpages/comments/g3qndt/psa_how_to_set_a_custom_new_tab_page_in_firefox/) to set a custom new tab page without an extension.

The post was written specifically for Firefox on Windows, so here are some notes for \*nix users:

- In step 1, go to your Firefox installation directory: if you are using Firefox, it will be `/usr/lib/firefox`, as for Librewolf it is `/usr/lib/librewolf`.

Follow the rest of the post **carefully** (just some personal experiences, nothing special :3)

#### Online:

1. Fork this repo
2. Host your forked repo somewhere (if you want to use GitHub pages, fork this to GitHub, or Codeberg, your choice)
3. Follow [this guide](https://docs.github.com/en/pages/getting-started-with-github-pages/creating-a-github-pages-site) to host your repo on GitHub, or [this guide](https://codeberg.page/) for Codeberg.
4.

- Use [Custom New Tab Page](https://addons.mozilla.org/en-US/firefox/addon/custom-new-tab-page/?src=search) for Firefox and enable "Force links to open in the top frame (experimental)" in the extension's preferences page
- Use [Custom New Tab URL](https://chrome.google.com/webstore/detail/custom-new-tab-url/mmjbdbjnoablegbkcklggeknkfcjkjia) for Chromium

If you had any problems, feel free to contact me on Discord (VnPower#5919).

## Customization

Customization is done directly in `config.js`. I also put some comments in there so you can understand what stuff do.

#### Buttons

You can use SVGs, image URLs for buttons.

Image from URLs will not change colors, unlike SVGs, which are grayscaled to work with hover animations.

```js
  buttons: [
    {
      name: "Discord",
      icon: `
        <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="50px" height="50px">
          <path
            d="M 18.90625 7 ..." />
        </svg>
      `,
      link: "https://discord.com/app"
    },
    {
      name: "VnPower's website",
      icon: `https://vnpower.exozy.me/avatar.png`,
      link: "https://vnpower.exozy.me"
    },
    {...}
```

## Credits

- [Bento](https://github.com/migueravila/Bento) - Inspiration
- [Icons8](https://icons8.com/) - Icons
- [Catppuccin](https://github.com/catppuccin/catppuccin), [Nord](https://nordtheme.com) - Color scheme
- wallhaven - Forest wallpaper
- 甘城なつき - Nachoneko's illust

## License

[MIT](./LICENSE)
